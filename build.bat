@echo off
REM
REM @file         build.bat
REM               Build the V8 library into the working directory.
REM @author       Jason Erb, jason@distributive.network
REM @date         December 2019

setLocal enableExtensions

set "argC=0"
for %%x in (%*) do set /A "argC+=1"
if %argC% lss 1 (
  echo Usage: %0 "<architecture>"
  exit /b 1
)
set "ARCHITECTURE=%~1"

pushd %~dp0
set /p VERSION_TAG=<version.txt || goto :exit
popd
if "%VERSION_TAG%"=="" (
   echo Version file 'version.txt' not found; aborting...
   exit /b 1
)
echo Building V8 tag "%VERSION_TAG%"...

if not exist depot_tools (
  echo Cloning depot tools...
  call git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git || goto :exit
)
set PATH=%CD%\depot_tools;%PATH%
set DEPOT_TOOLS_WIN_TOOLCHAIN=0
set GYP_MSVS_VERSION=2019

if not exist v8 (
  echo Fetching V8...
  call fetch v8 || goto :exit
)
pushd v8

echo Checking out "%VERSION_TAG%"...
call git checkout "%VERSION_TAG%" || goto :exit

echo Syncing gclient...
call gclient sync -v -D --force --reset || goto :exit

echo Installing pywin32...
call python -m pip install pywin32 || goto :exit

echo Copying files to install...
robocopy /np . ..\install "LICENSE"
if %ERRORLEVEL% GEQ 8 goto :exit
robocopy /np .\include ..\install\include /mt /S
if %ERRORLEVEL% GEQ 8 goto :exit

echo Building "%ARCHITECTURE%"...
call :build debug || goto :exit
call :build release || goto :exit

popd
exit /b 0

:build
set configuration=%~1

set args=
set args=%args% is_clang=false
set args=%args% is_component_build=false
if "%configuration%"=="release" (
  set args=%args% is_debug=false
)
set args=%args% target_cpu=""%ARCHITECTURE%""
set args=%args% use_custom_libcxx=false
set args=%args% v8_monolithic=true
set args=%args% v8_static_library=true
set args=%args% v8_use_external_startup_data=false

echo Generating "%ARCHITECTURE%.%configuration%"...
call gn gen "..\artifacts\lib\%ARCHITECTURE%.%configuration%" --args="%args%" || goto :exit

echo Building "%ARCHITECTURE%.%configuration%"...
call autoninja -C "..\artifacts\lib\%ARCHITECTURE%.%configuration%" v8_monolith || goto :exit

echo Copying "%ARCHITECTURE%.%configuration%" files to install...
robocopy /np ..\artifacts\lib\%ARCHITECTURE%.%configuration%\obj ..\install\lib\%ARCHITECTURE%.%configuration%\obj "v8_monolith.lib"
if %ERRORLEVEL% GEQ 8 goto :exit
robocopy /np ..\artifacts\lib\%ARCHITECTURE%.%configuration%\obj ..\install\lib\%ARCHITECTURE%.%configuration%\obj "*.pdb" /mt /S
if %ERRORLEVEL% GEQ 8 goto :exit
robocopy /np ..\artifacts\lib\%ARCHITECTURE%.%configuration% ..\install\lib\%ARCHITECTURE%.%configuration% "icudtl.dat"
if %ERRORLEVEL% GEQ 8 goto :exit
exit /b 0

:exit
exit /b %ERRORLEVEL%
