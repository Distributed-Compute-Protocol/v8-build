# INTRODUCTION

This repository contains scripts to build and release V8 binaries.

## Setting Up

Before building, ensure that your system is set up as required:

* Linux:
  * https://chromium.googlesource.com/chromium/src/+/master/docs/linux/build_instructions.md#system-requirements
* Windows:
  * https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md#system-requirements
  * https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md#Setting-up-Windows
* Mac:
  * https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md#system-requirements
  * [Python 2](https://www.python.org/downloads/release/python-2718/) is
    additionally required.
  * Run `xcode-select --install` in the terminal to ensure command-line tools
    are installed.
  * MacOS 10.15 SDK is required. From https://stackoverflow.com/a/66497078:
    * Download
      [Xcode 12.1](https://download.developer.apple.com/Developer_Tools/Xcode_12.1/Xcode_12.1.xip)
      and double-click to decompress the Xcode.app application bundle.
    * Right-click Xcode.app and select "Show Package Contents". From the
      "Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs"
      subdirectory, copy the "MacOSX.sdk" folder to a temporary location.
    * Rename the copied "MacOSX.sdk" folder to "MacOSX10.15.sdk".
    * Right-click /Applications/Xcode.app and select "Show Package Contents".
      Into the "Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs"
      subdirectory, move the "MacOSX10.15.sdk" folder from the temporary
      location.

## Getting The Code

To clone the
[V8 Build repository](https://gitlab.com/Distributed-Compute-Protocol/v8-build),
enter the following:

    git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/v8-build.git

## Building

To build, enter the following:

    mkdir build
    cd build
    cmake ..
    cmake --build .

## Releasing

To create a build release, enter the following:

    ./release.sh <build-major>.<build-minor>.<build-patch>

The build version is distinct from the version of V8 it is building due to
GitLab version format limitations. Since build version 8.0.0 inclusive, the
build version is formulated as follows:

* The build major version corresponds to a unique V8 version. Any V8 version
  change requires a corresponding build major version change, along with
  specifying the new V8 version in the "version.txt" file.
* The build minor version increments for a potentially breaking change to the
  build code in this repository (for example, changing a build option).
* The build patch version increments for non-breaking changes to the build code
  in this repository.

See the
[releases page](https://gitlab.com/Distributed-Compute-Protocol/v8-build/-/releases)
for existing versions.

The release script will cause a build version tag to be pushed, which in turn
will cause the build artifacts to be uploaded into the
[package registry page](https://gitlab.com/Distributed-Compute-Protocol/v8-build/-/packages),
and the release to appear on the
[releases page](https://gitlab.com/Distributed-Compute-Protocol/v8-build/-/releases).

The
[package registry page](https://gitlab.com/Distributed-Compute-Protocol/v8-build/-/packages)
contains public links to the build packages and SHA256 hash files, which can be
used by any clients wishing to download the builds.
