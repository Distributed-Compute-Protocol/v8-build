#!/bin/bash
#
# @file         build.sh
#               Build the V8 library into the working directory.
# @author       Jason Erb, jason@distributive.network
# @date         December 2019

set -e

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <architecture>" >&2
  exit 1
fi
ARCHITECTURE=$1

pushd "$(dirname "$0")"
VERSION_TAG=$(cat version.txt)
popd
if [ -z "${VERSION_TAG}" ]; then
  echo "Version file 'version.txt' not found; aborting..."
  exit 1
fi
OS="$(uname -s)"
echo "Building V8 tag \"${VERSION_TAG}\" on OS \"${OS}\"..."

if [ ! -d depot_tools ]; then
  echo "Cloning depot tools..."
  git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
fi
export PATH=${PWD}/depot_tools:$PATH

if [ ! -d v8 ]; then
  echo "Fetching V8..."
  fetch v8
fi
pushd v8

echo "Checking out \"${VERSION_TAG}\"..."
git checkout "${VERSION_TAG}"

echo "Syncing gclient..."
gclient sync -D

if [ "${OS}" = "Linux" ]; then
  pushd build

  echo "Installing Linux build dependencies..."
  # Snapcraft installation fails inside a docker container; patch to remove
  sed -i 's/${dev_list} snapcraft/${dev_list}/g' ./install-build-deps.sh
  ./install-build-deps.sh --no-prompt
  git reset --hard HEAD

  if [ "${ARCHITECTURE}" = "arm64" ]; then
    echo "Prepping arm64 sysroot tools..."
    linux/sysroot_scripts/install-sysroot.py --arch=arm64
  fi

  popd
fi

echo "Copying files to install..."
mkdir -p ../install
cp LICENSE ../install/
cp -r include ../install/

echo "Building \"${ARCHITECTURE}\"..."
build() {
  configuration=$1

  args=""
  args="${args} icu_use_data_file=false"
  args="${args} is_component_build=false"
  if [ "${configuration}" = "release" ]; then
    args="${args} is_debug=false"

    # Required to keep the binary size down.
    args="${args} strip_debug_info=true"
    args="${args} symbol_level=0"
  fi
  args="${args} target_cpu=\"${ARCHITECTURE}\""
  args="${args} use_custom_libcxx=false"
  args="${args} v8_monolithic=true"
  args="${args} v8_static_library=true"
  args="${args} v8_use_external_startup_data=false"

  echo "Generating \"${ARCHITECTURE}.${configuration}\"..."
  gn gen "../artifacts/lib/${ARCHITECTURE}.${configuration}" --args="${args}"

  echo "Building \"${ARCHITECTURE}.${configuration}\"..."
  autoninja -C "../artifacts/lib/${ARCHITECTURE}.${configuration}" v8_monolith

  echo "Copying \"${ARCHITECTURE}.${configuration}\" files to install..."
  pushd ../artifacts
  rsync -R "lib/${ARCHITECTURE}.${configuration}/obj/libv8_monolith.a" ../install/
  popd
}
build debug
build release

popd
